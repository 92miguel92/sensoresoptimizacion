package moviles.eps.ua.es.sensoresoptimizacion;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;

/**
 * Created by mastermoviles on 22/02/2017.
 */

public class LocalizationActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private TextView latitud_tv, longitud_tv, weather_tv, temperature_tv, humidity_tv, pressure_tv, province_tv, address_tv, municipity_tv, country_tv;
    protected Location mLastLocation;

    protected GoogleApiClient apiClient;

    protected synchronized void buildGoogleApiClient() {
        apiClient = new GoogleApiClient.Builder(this).
                enableAutoManage(this, this).
                addConnectionCallbacks(this).
                addApi(LocationServices.API).build();
    }

    class RetrieveDataWeather extends AsyncTask<String, Void, InputStream>{

        @Override
        protected InputStream doInBackground(String... urls) {
            HttpURLConnection con = null;
            InputStream is = null;

            try {
                con = (HttpURLConnection) ( new URL(urls[0])).openConnection();
                con.setRequestMethod("GET");
                con.setDoInput(true);
                con.connect();
                is = con.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return(is);
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }
        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        setTitle("Localización del dispositivo");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        latitud_tv = (TextView) findViewById(R.id.latitude_tv);
        longitud_tv = (TextView) findViewById(R.id.longitude_tv);
        weather_tv = (TextView) findViewById(R.id.weather);
        temperature_tv = (TextView) findViewById(R.id.temperature);
        humidity_tv = (TextView) findViewById(R.id.humidity);
        pressure_tv = (TextView) findViewById(R.id.pressure);
        province_tv = (TextView) findViewById(R.id.province);
        address_tv = (TextView) findViewById(R.id.address);
        municipity_tv = (TextView) findViewById(R.id.municipality);
        country_tv = (TextView) findViewById(R.id.country);

        buildGoogleApiClient();
    }
    
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);

        if(mLastLocation != null){
            latitud_tv.setText(String.valueOf(mLastLocation.getLatitude()));
            longitud_tv.setText(String.valueOf(mLastLocation.getLongitude()));
        }

        Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> address = geoCoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
            int maxLines = address.get(0).getMaxAddressLineIndex();
            for (int i=0; i<=maxLines; i++) {
                if(maxLines == 2){
                    switch(i){
                        case 0: municipity_tv.setText(address.get(0).getAddressLine(i));
                            break;
                        case 1: province_tv.setText(address.get(0).getAddressLine(i));
                            break;
                        case 2: country_tv.setText(address.get(0).getAddressLine(i));
                            break;
                    }
                }else if(maxLines == 3) {
                    switch(i){
                        case 0: address_tv.setText(address.get(0).getAddressLine(i));
                            break;
                        case 1: municipity_tv.setText(address.get(0).getAddressLine(i));
                            break;
                        case 2: province_tv.setText(address.get(0).getAddressLine(i));
                            break;
                        case 3: country_tv.setText(address.get(0).getAddressLine(i));
                            break;
                    }
                }
            }
            if(municipity_tv.getText() != null){
                String[] cod_mun = municipity_tv.getText().toString().split(" ");
                if (cod_mun.length > 0) {
                    String localization = cod_mun[1];
                    String dataWeather = queryWeather(localization);
                    String code = setWeather(dataWeather);
                    if(code.length()>0){
                        getImage(code);
                        //Bitmap img = BitmapFactory.decodeByteArray(imageBytes,0,imageBytes.length);
                        //iconWeather.setImageBitmap(img);
                    }else{
                        Toast.makeText(this,"Error to get Image",Toast.LENGTH_SHORT);
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("Error: "+e.getMessage());
        } catch (NullPointerException e) {
            System.out.println("Error: "+e.getMessage());
        }
    }

    public String queryWeather(String localization){
        String request = "http://api.openweathermap.org/data/2.5/weather?lang=es&units=metric&appid=1adb13e22f23c3de1ca37f3be90763a9&q="+localization;

        HttpURLConnection con = null ;
        InputStream is = null;

        try {
            is = new RetrieveDataWeather().execute(request).get();

            // Let's read the response
            StringBuffer buffer = new StringBuffer();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while ( (line = br.readLine()) != null )
                buffer.append(line + "rn");

            is.close();
            return buffer.toString();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }

        return null;
    }

    public byte[] getImage(String code) {
        String baseIcon = "http://openweathermap.org/img/w/";

        HttpURLConnection con = null ;
        InputStream is = null;
        try {
            new DownloadImageTask((ImageView) findViewById(R.id.img_weather)).execute(baseIcon + code+".png");
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }

        return null;

    }

    public String setWeather(String data){

        try {
            JSONObject jObj = new JSONObject(data);
            JSONArray jArrWeather = jObj.getJSONArray("weather");
            JSONObject JSONWeather = jArrWeather.getJSONObject(0);
            JSONObject mainObj = jObj.getJSONObject("main");
            temperature_tv.setText(mainObj.getString("temp")+" ºC");
            humidity_tv.setText(mainObj.getString("humidity")+" %");
            pressure_tv.setText(mainObj.getString("pressure")+" Pa");
            weather_tv.setText(JSONWeather.getString("description"));
            return JSONWeather.getString("icon");
        } catch (JSONException e) {
            System.out.println("Error: "+e.getMessage());
        }

        return "";
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }
}
