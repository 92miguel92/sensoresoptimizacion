/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package moviles.eps.ua.es.sensoresoptimizacion;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.hardware.*;
import android.hardware.Sensor;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private int powerBattery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the content of the activity to use the activity_main.xml layout file
        setContentView(R.layout.activity_main);


        // find the view that shows the numbers category
        TextView listado =(TextView) findViewById(R.id.listado);
        TextView RAMarrayList = (TextView) findViewById(R.id.arrayList);
        TextView RAMtextView = (TextView) findViewById(R.id.textView);
        TextView movimiento = (TextView) findViewById(R.id.movimiento);
        TextView posicion = (TextView) findViewById(R.id.posicion);
        TextView ambientales = (TextView) findViewById(R.id.ambientales);
        TextView bateria = (TextView) findViewById(R.id.battery);
        TextView localizacion = (TextView) findViewById(R.id.localization);

        // Set a clicklistener on taht View
        listado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listadoIntent=new Intent (MainActivity.this, Listado.class);
                startActivity(listadoIntent);
            }
        });
        RAMarrayList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listadoIntent=new Intent (MainActivity.this, ArrayListActivity.class);
                startActivity(listadoIntent);
            }
        });
        RAMtextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listadoIntent=new Intent (MainActivity.this, TextViewActivity.class);
                startActivity(listadoIntent);
            }
        });
        posicion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkForPower() || powerBattery > 50){
                    Intent listadoIntent=new Intent (MainActivity.this, SensoresListener.class);
                    ArrayList<Integer> sensores = new ArrayList<>(Arrays.asList(Sensor.TYPE_GAME_ROTATION_VECTOR,Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR,
                            Sensor.TYPE_MAGNETIC_FIELD, Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED, Sensor.TYPE_PROXIMITY));
                    listadoIntent.putExtra("listaEventos", sensores);
                    listadoIntent.putExtra("title", "Sensores de Posición");
                    startActivity(listadoIntent);
                }else{
                    Toast.makeText(getApplicationContext(),"<regla> <EL MÓVIL SE ESTÁ CARGANDO O EL NIVEL DE BATERÍA SEA MAYOR DEL 50%/>",Toast.LENGTH_LONG).show();
                }
            }
        });
        movimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listadoIntent=new Intent (MainActivity.this, SensoresListener.class);
                ArrayList<Integer> sensores = new ArrayList<>(Arrays.asList(Sensor.TYPE_GRAVITY,Sensor.TYPE_LINEAR_ACCELERATION,Sensor.TYPE_ROTATION_VECTOR,
                        Sensor.TYPE_SIGNIFICANT_MOTION,Sensor.TYPE_STEP_COUNTER,Sensor.TYPE_STEP_DETECTOR,Sensor.TYPE_ACCELEROMETER,
                        Sensor.TYPE_GYROSCOPE,Sensor.TYPE_GYROSCOPE_UNCALIBRATED));
                listadoIntent.putExtra("listaEventos", sensores);
                listadoIntent.putExtra("title", "Sensores de Movimiento");
                startActivity(listadoIntent);
            }
        });
        ambientales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listadoIntent=new Intent (MainActivity.this, SensoresListener.class);
                ArrayList<Integer> sensores = new ArrayList<>(Arrays.asList(Sensor.TYPE_PRESSURE, Sensor.TYPE_AMBIENT_TEMPERATURE, Sensor.TYPE_LIGHT,
                        Sensor.TYPE_RELATIVE_HUMIDITY));
                listadoIntent.putExtra("listaEventos", sensores);
                listadoIntent.putExtra("title", "Sensores Ambientales");
                startActivity(listadoIntent);
            }
        });
        bateria.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent listadoIntent=new Intent (MainActivity.this, BatteryActivity.class);
                startActivity(listadoIntent);
            }
        });
        localizacion.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent listadoIntent=new Intent (MainActivity.this, LocalizationActivity.class);
                startActivity(listadoIntent);
            }
        });
    }

    private boolean checkForPower(){
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = this.registerReceiver(null, filter);
        getBatteryPowerLevel(batteryStatus);
        //registerReceiver(batteryInfoReceiver, filter);

        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = (chargePlug == BatteryManager.BATTERY_PLUGGED_USB);
        boolean acCharge = (chargePlug == BatteryManager.BATTERY_PLUGGED_AC);
        boolean wirelessCharge = false;

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR1) {
            wirelessCharge = (chargePlug == BatteryManager.BATTERY_PLUGGED_WIRELESS);
        }

        return(usbCharge || acCharge || wirelessCharge);
    }

    public int getBatteryPowerLevel(Intent intent) {
        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float batteryPct = 100 * level / (float) scale;
        Log.i(BatteryActivity.class.getName(), "battery level_ " + batteryPct);
        powerBattery = (int)batteryPct;
        return (int)batteryPct;
    }
}
