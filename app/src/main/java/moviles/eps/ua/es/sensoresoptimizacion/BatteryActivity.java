package moviles.eps.ua.es.sensoresoptimizacion;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by mastermoviles on 20/02/2017.
 */

public class BatteryActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private TextView batteryLevel, batteryDock, batteryCharge, batteryHealth, batteryTemp,
    batteryVol, batteryTecnology, batteryState;

    public String getBatteryPluggedInfo(Intent intent) {
        String msg = "Not charging";
        switch (intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1))
        {
            case BatteryManager.BATTERY_PLUGGED_AC:
                msg="AC charging";
                break;
            case BatteryManager.BATTERY_PLUGGED_USB:
                msg="USB charging";
                break;
            default:
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    if(intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1)==BatteryManager.BATTERY_PLUGGED_WIRELESS) {
                        msg = "Wireless charging";
                    }
                }
                break;
        }
        return msg;
    }

    public int getBatteryPowerLevel(Intent intent) {
        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float batteryPct = 100 * level / (float) scale;
        Log.i(BatteryActivity.class.getName(), "battery level_ " + batteryPct);
        return (int)batteryPct;
    }

    public String getBatteryDockState(Intent intent) {
        String msg ="None";

        switch (intent.getIntExtra(Intent.EXTRA_DOCK_STATE, -1))
        {
            case Intent.EXTRA_DOCK_STATE_UNDOCKED:
                msg = "Undocked";
                break;
            case Intent.EXTRA_DOCK_STATE_CAR:
                msg="Car";
                break;
            case Intent.EXTRA_DOCK_STATE_DESK:
                msg= "Desk";
                break;
            case Intent.EXTRA_DOCK_STATE_HE_DESK:
                msg="Low range deskt (analogic)";
                break;
            case Intent.EXTRA_DOCK_STATE_LE_DESK:
                msg="High range desk (digital)";
                break;
        }
        return msg;
    }

    public String getHealthOfBattery(Intent intent) {
        String health = "";

        switch(intent.getIntExtra(BatteryManager.EXTRA_HEALTH,-1)) {
            case BatteryManager.BATTERY_HEALTH_UNKNOWN:
                health = "Unknown";
                break;
            case BatteryManager.BATTERY_HEALTH_GOOD:
                health = "Good";
                break;
            case BatteryManager.BATTERY_HEALTH_OVERHEAT:
                health = "Overheat";
                break;
            case BatteryManager.BATTERY_HEALTH_DEAD:
                health = "Dead";
                break;
            case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE:
                health = "Over voltage";
                break;
            case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE:
                health = "Unspecified failure";
                break;
            case BatteryManager.BATTERY_HEALTH_COLD:
                health = "Cold";
                break;
        }
        return health;
    }

    private String getBatteryStatus(Intent intent) {
        String statusString = "Unknown";
        switch (intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1)) {
            case BatteryManager.BATTERY_STATUS_CHARGING:
                statusString = "Charging";
                break;
            case BatteryManager.BATTERY_STATUS_DISCHARGING:
                statusString = "Discharging";
                break;
            case BatteryManager.BATTERY_STATUS_FULL:
                statusString = "Full";
                break;
            case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                statusString = "Not Charging";
                break;
        }
        return statusString;
    }

    private int mProgressStatus = 0;
    private Handler mHandler = new Handler();

    private BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final int powerBattery = getBatteryPowerLevel(intent);
            progressBar.setProgress(powerBattery);
            batteryLevel.setText(powerBattery + " %");
            batteryCharge.setText(getBatteryPluggedInfo(intent));
            batteryDock.setText(getBatteryDockState(intent));
            batteryHealth.setText(getHealthOfBattery(intent));
            batteryState.setText(getBatteryStatus(intent));
            Log.i(BatteryActivity.class.getName(), "tech: " + intent.getStringExtra(BatteryManager.EXTRA_TECHNOLOGY));
            batteryTemp.setText( (( intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0) / 10.f) + " ºC") );
            batteryVol.setText( ((intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0) / 1000.f)+ " V") );
            String tech = intent.getStringExtra(BatteryManager.EXTRA_TECHNOLOGY);
            if(tech.isEmpty()) tech= "Unknown";
            batteryTecnology.setText(tech);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battery);

        setTitle("Estado de la batería");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        batteryLevel = (TextView)findViewById(R.id.batteryLevel);
        batteryDock = (TextView)findViewById(R.id.batteryDock);
        batteryCharge = (TextView)findViewById(R.id.batteryCharge);
        batteryHealth = (TextView)findViewById(R.id.batteryState);
        batteryTemp = (TextView)findViewById(R.id.batteryTemp);
        batteryVol = (TextView)findViewById(R.id.batteryVol);
        batteryTecnology = (TextView)findViewById(R.id.batteryTechnology);
        batteryState = (TextView)findViewById(R.id.batteryStatus);

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_BATTERY_CHANGED);
        filter.addAction(Intent.ACTION_DOCK_EVENT);
        filter.addAction(Intent.ACTION_BATTERY_LOW);
        filter.addAction(Intent.ACTION_BATTERY_OKAY);
        registerReceiver(batteryInfoReceiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();

        unregisterReceiver(batteryInfoReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }
}
