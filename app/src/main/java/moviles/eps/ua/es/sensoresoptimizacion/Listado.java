package moviles.eps.ua.es.sensoresoptimizacion;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Listado extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayout actividad_listado = (LinearLayout) findViewById(R.id.vista_listado);

        ////// muestra lista de sensores
        SensorManager mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        List<Sensor> listaSensores = mSensorManager.getSensorList(Sensor.TYPE_ALL);


        for (Sensor sensor : listaSensores) {
            TextView listadoView = new TextView(this);
            listadoView.setText(sensor.getName());
            Log.d("sensores", "" + sensor.getName());
            actividad_listado.addView(listadoView);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }
}
