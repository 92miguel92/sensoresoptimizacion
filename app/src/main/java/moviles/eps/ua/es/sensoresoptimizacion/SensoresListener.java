package moviles.eps.ua.es.sensoresoptimizacion;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SensoresListener extends AppCompatActivity{

    private SensorManager mSensorManager;
    private List<moviles.eps.ua.es.sensoresoptimizacion.Sensor> listaSensores;
    private List<Integer> listaNombreSensores;
    private List<Integer> listaTiposDisponibles;
    private SensorEventListener mSensorListener;
    private movementAdapter call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_list);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        setTitle(getIntent().getStringExtra("title"));
        listaSensores = new ArrayList<>();
        listaNombreSensores = new ArrayList<>();
        listaTiposDisponibles = new ArrayList<>();
        call = new movementAdapter(listaSensores);

        Intent intent = getIntent();
        List<Integer> sensores = intent.getIntegerArrayListExtra("listaEventos");
        listaNombreSensores = sensores;
    }

    @Override
    protected void onResume() {
        super.onResume();

        final ListView list = (ListView) findViewById(R.id.vista_list);

        for(int i = 0; i<listaNombreSensores.size(); i++){
            if(!mSensorManager.getSensorList(listaNombreSensores.get(i)).isEmpty()){
                final Sensor sensor = mSensorManager.getSensorList(listaNombreSensores.get(i)).get(0);
                final moviles.eps.ua.es.sensoresoptimizacion.Sensor sensorPropio = new moviles.eps.ua.es.sensoresoptimizacion.Sensor();
                sensorPropio.setName(sensor.getName());
                sensorPropio.setPower(sensor.getPower());
                sensorPropio.setType(sensor.getType());
                listaSensores.add(sensorPropio);
                listaTiposDisponibles.add(sensor.getType());
                mSensorListener = new SensorEventListener() {

                    @Override
                    public void onAccuracyChanged(Sensor arg0, int arg1) {
                    }

                    @Override
                    public void onSensorChanged(SensorEvent event) {
                        for(int i=0; i<listaTiposDisponibles.size();i++){
                            if(event.sensor.getType() == listaTiposDisponibles.get(i)){
                                listaSensores.get(i).setX(event.values[0]);
                                if(event.values.length>1){
                                    listaSensores.get(i).setY(event.values[1]);
                                }else{
                                    listaSensores.get(i).setY(0.0f);
                                }
                                if(event.values.length>2){
                                    listaSensores.get(i).setZ(event.values[2]);
                                }else{
                                    listaSensores.get(i).setZ(0.0f);
                                }
                            }
                        }
                        list.setAdapter(call);
                    }
                };
                mSensorManager.registerListener(mSensorListener, sensor, mSensorManager.SENSOR_DELAY_UI);
            }
        }
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(mSensorListener);
        super.onPause();
    }

    class movementAdapter extends BaseAdapter{

        private List<moviles.eps.ua.es.sensoresoptimizacion.Sensor> listaSensor;
        private TextView nameSensor, powerSensor, xSensor, ySensor, zSensor;

        public movementAdapter(List<moviles.eps.ua.es.sensoresoptimizacion.Sensor> listaSensor){
            this.listaSensor = listaSensor;
        }

        @Override
        public int getCount() {
            return this.listaSensor.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = getLayoutInflater();
            View row;
            String stringX = "",stringY = "",stringZ = "";
            row = inflater.inflate(R.layout.sensor_event, parent, false);

            nameSensor = (TextView) row.findViewById(R.id.sensorName);
            powerSensor = (TextView) row.findViewById(R.id.sensorPower);
            xSensor = (TextView) row.findViewById(R.id.txtAccX);
            ySensor = (TextView) row.findViewById(R.id.txtAccY);
            zSensor = (TextView) row.findViewById(R.id.txtAccZ);
            nameSensor.setText("Name: "+listaSensor.get(position).getName());
            powerSensor.setText("Power: "+String.format(Locale.US,"%.5f",listaSensor.get(position).getPower())+" mA");

            stringX = "X: "+String.format(Locale.US,"%.3f",listaSensor.get(position).getX());
            stringY = "Y: "+String.format(Locale.US,"%.3f",listaSensor.get(position).getY());
            stringZ = "Z: "+String.format(Locale.US,"%.3f",listaSensor.get(position).getZ());

            switch(listaSensor.get(position).getType()){
                case Sensor.TYPE_MAGNETIC_FIELD:
                case Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED:
                    stringX += " µT";
                    stringY += " µT";
                    stringZ += " µT";
                    break;
                case Sensor.TYPE_PROXIMITY:
                    stringX += " cm";
                    stringY += " cm";
                    stringZ += " cm";
                    break;
                case Sensor.TYPE_AMBIENT_TEMPERATURE:
                    stringX += " ºC";
                    stringY += " ºC";
                    stringZ += " ºC";
                    break;
                case Sensor.TYPE_LIGHT:
                    stringX += " lx";
                    stringY += " lx";
                    stringZ += " lx";
                    break;
                case Sensor.TYPE_PRESSURE:
                    stringX += " mbar";
                    stringY += " mbar";
                    stringZ += " mbar";
                    break;
                case Sensor.TYPE_RELATIVE_HUMIDITY:
                    stringX += " %";
                    stringY += " %";
                    stringZ += " %";
                    break;
                case Sensor.TYPE_ACCELEROMETER:
                case Sensor.TYPE_GRAVITY:
                case Sensor.TYPE_LINEAR_ACCELERATION:
                    stringX += " m/s^2";
                    stringY += " m/s^2";
                    stringZ += " m/s^2";
                    break;
                case Sensor.TYPE_GYROSCOPE:
                case Sensor.TYPE_GYROSCOPE_UNCALIBRATED:
                    stringX += " rad/s";
                    stringY += " rad/s";
                    stringZ += " rad/s";
                    break;
                case Sensor.TYPE_STEP_COUNTER:
                    stringX += " Steps";
                    stringY += " Steps";
                    stringZ += " Steps";
                    break;

            }

            xSensor.setText(stringX);
            ySensor.setText(stringY);
            zSensor.setText(stringZ);

            return (row);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }
}