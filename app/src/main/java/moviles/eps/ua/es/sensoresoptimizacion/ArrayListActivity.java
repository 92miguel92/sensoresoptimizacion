package moviles.eps.ua.es.sensoresoptimizacion;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by mastermoviles on 08/02/2017.
 */

public class ArrayListActivity extends AppCompatActivity {

    private final String QUIJOTE = "En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo que vivía" +
            " un hidalgo de los de lanza en astillero, adarga antigua, rocín flaco y galgo corredor. Una olla de algo más vaca que" +
            " carnero, salpicón las más noches, duelos y quebrantos los sábados, lentejas los viernes, algún palomino de añadidura" +
            " los domingos, consumían las tres partes de su hacienda. El resto della concluían sayo de velarte, calzas de velludo" +
            " para las fiestas con sus pantuflos de lo mismo, los días de entre semana se honraba con su vellori de lo más fino." +
            " Tenía en su casa una ama que pasaba de los cuarenta, y una sobrina que no llegaba a los veinte, y un mozo de campo" +
            " y plaza, que así ensillaba el rocín como tomaba la podadera. Frisaba la edad de nuestro hidalgo con los cincuenta" +
            " años, era de complexión recia, seco de carnes, enjuto de rostro; gran madrugador y amigo de la caza. Quieren decir" +
            " que tenía el sobrenombre de Quijada o Quesada (que en esto hay alguna diferencia en los autores que deste caso escriben)," +
            " aunque por conjeturas verosímiles se deja entender que se llama Quijana; pero esto importa poco a nuestro cuento;" +
            " basta que en la narración dél no se salga un punto de la verdad";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ListView list = (ListView)findViewById(R.id.vista_list);

        ArrayList<String> words = new ArrayList<String>();

        // mostrar consumo de ram con listado de 1000 palabras
        for (int i = 0; i < 1000; i++) {
            words.add(QUIJOTE + i);

        }
        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, words);
        list.setAdapter(itemsAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }
}
