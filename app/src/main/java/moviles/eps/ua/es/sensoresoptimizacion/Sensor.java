package moviles.eps.ua.es.sensoresoptimizacion;

import java.io.Serializable;

/**
 * Created by mastermoviles on 19/02/2017.
 */

public class Sensor implements Serializable {

    private String name;
    private float power, x,y,z;
    private int type;

    public Sensor(){
        name = "";
        power = 0;
        type = 0;

        x = 0;
        y = 0;
        z = 0;
    }

    public Sensor(String name, float power, float x, float y, float z) {
        this.name = name;
        this.power = power;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPower() {
        return power;
    }

    public void setPower(float power) {
        this.power = power;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
